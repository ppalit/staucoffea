# staucoffea



## Getting started


## Name
This is an initial framework for Stau analysis using Run 3 NANOAOD following [bucoffea](https://github.com/bu-cms/bucoffea) framework.


## Installation

```
git clone -b stau_nano_run3 https://gitlab.cern.ch/ppalit/staucoffea.git
cd staucoffea

source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3/x86_64-centos7-gcc8-opt/setup.sh
ENVNAME="cmucoffeaenv"
python -m venv ${ENVNAME}
source ${ENVNAME}/bin/activate
export PYTHONPATH=${PWD}/${ENVNAME}/lib/python3.6/site-packages:$PYTHONPATH

pip install -r cmucoffea/requirements.txt
pip install numpy==1.19.5
pip install matplotlib==3.1
pip install mplhep==0.1.5

cd cmucoffea/cmucoffea

touch __init__.py
```

Everytime you enter the directory, you have to activate `env.sh` which will contain the following line : 

```
source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3/x86_64-centos7-gcc8-opt/setup.sh
ENVNAME="cmucoffeaenv"
DIRNAME="cmucoffea"
source ${ENVNAME}/bin/activate
export PYTHONPATH=${PWD}/${ENVNAME}/lib/python3.6/site-packages:$PYTHONPATH
export PYTHONPATH=${PWD}/${DIRNAME}/:$PYTHONPATH
export PYTHONPATH=${PWD}/${DIRNAME}/${DIRNAME}/:$PYTHONPATH
```

## Usage

`stau` folder contains the main analyzers. `stau/definitions.py` contains the declaration of histograms, trees, cutflow and nanoaod objects, while `stau/stauProcessor.py` contains the gencandidate object, object selection, event selection, filling of cutflow, histograms amd trees for both prompt and displaced analysis. 

Input parameters (cuts, trigger names, weight files etc) will be provided by yaml files in `config/`, such as, `stau_disp.yaml` for displaced analysis.

Using the processor, the local execution will be done by executor in : `scripts/run_quick.py` , which takes one displaced tau signal and DY+jets background as input samples : currently from eos. Chunksize = 10000 , better to use the whole chunk (total eventsize ) to understand while debugging.
Output is generated as cutflow in .txt file and other accumulators in .coffea file

To print the cutflow and generate histograms : 

```
./scripts/run_quick.py stau_disp(stau_prompt) 
```

Normalized plots can be printed by : `scripts/print_plots_norm.py`

```
./scripts/print_plots_norm.py {signal}.coffea {background}.coffea --region='sr_prompt'/'sr_displaced' --outpath='./out'
``` 


## Support
In case you face problem in pip installing like this : `_NamespacePath object has no attribute sort`, you may take help from this solution : 
`https://github.com/pypa/setuptools/issues/885`

```
source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc11-opt/setup.sh
NAME=cmucoffeaenv
LCG=/cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc11-opt/
python -m venv --copies $NAME
source $NAME/bin/activate
LOCALPATH=$NAME$(python -c 'import sys; print(f"/lib/python{sys.version_info.major}.{sys.version_info.minor}/site-packages")')
export PYTHONPATH=${LOCALPATH}:$PYTHONPATH
python -m pip install setuptools pip wheel --upgrade
python -m pip install -r cmucoffea/requirements.txt
sed -i '1s/#!.*python$/#!\/usr\/bin\/env python/' $NAME/bin/*
sed -i '40s/.*/VIRTUAL_ENV="$(cd "$(dirname "$(dirname "${BASH_SOURCE[0]}" )")" \&\& pwd)"/' $NAME/bin/activate
sed -i "2a source ${LCG}/setup.sh" $NAME/bin/activate
sed -i "3a export PYTHONPATH=${LOCALPATH}:\$PYTHONPATH" $NAME/bin/activate
export PYTHONPATH=cmucoffeaenv/lib/python3.9/site-packages/:$PYTHONPATH
DIRNAME="cmucoffea"
export PYTHONPATH=${PWD}/${DIRNAME}/:$PYTHONPATH
export PYTHONPATH=${PWD}/${DIRNAME}/${DIRNAME}/:$PYTHONPATH
```





